package stepdefs;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.serenitybdd.screenplay.actions.Open;
import screenplay.pages.DuckDuckGoHomePage;
import screenplay.pages.RedditPage;
import screenplay.questions.Reddit;
import screenplay.tasks.Search;
import screenplay.tasks.ViewThe;

import static net.serenitybdd.screenplay.GivenWhenThen.*;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static net.serenitybdd.screenplay.matchers.ConsequenceMatchers.*;
import static org.hamcrest.Matchers.*;

public class SearchSteps {

    private DuckDuckGoHomePage duckDuckGoHomePage = new DuckDuckGoHomePage();
    private RedditPage redditPage = new RedditPage();

    @Given("(.*) is able to use DuckDuckGo for searching$")
    public void is_able_to_use_Google_for_searching(String actor) {
        theActorCalled(actor).wasAbleTo(Open.browserOn().the(duckDuckGoHomePage));
    }

    @When("^(?:she|he|they) searche?s? for \"([^\"]*)\"$")
    public void sheSearchesFor(String searchTerm) {
        theActorInTheSpotlight()
                .attemptsTo(Search.forTerm(searchTerm));
    }

    @And("^(?:she|he|they) views? the first result$")
    public void sheViewsTheFirstResult() {
        theActorInTheSpotlight()
                .attemptsTo(ViewThe.firstSearchResult());
    }

    @Then("^(?:she|he|they) should be on the Reddit homepage$")
    public void sheShouldBeOnTheRedditHomepage() {
        theActorInTheSpotlight()
                .should(seeThat(Reddit.loaded(),
                        displays("title", equalTo("reddit: the front page of the internet"))));
    }
}
