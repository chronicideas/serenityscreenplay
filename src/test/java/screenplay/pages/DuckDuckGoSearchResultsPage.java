package screenplay.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;

import java.util.List;

public class DuckDuckGoSearchResultsPage extends DuckDuckGoHomePage {

    @FindBy(className = "result__a")
    private List<WebElementFacade> searchResultLinks;

    public RedditPage selectFirstResult() {
        searchResultLinks.get(0).click();
        return new RedditPage();
    }
}
