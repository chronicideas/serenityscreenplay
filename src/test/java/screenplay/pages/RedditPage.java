package screenplay.pages;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.WebElementFacade;

public class RedditPage extends DuckDuckGoSearchResultsPage {

    @FindBy(className = "promotedLink")
    public
    WebElementFacade promotedLink;
}
