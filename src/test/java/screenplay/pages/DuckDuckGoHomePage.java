package screenplay.pages;

import net.serenitybdd.core.pages.WebElementFacade;
import net.serenitybdd.screenplay.targets.Target;
import net.thucydides.core.annotations.DefaultUrl;
import net.thucydides.core.pages.PageObject;

@DefaultUrl("https://start.duckduckgo.com")
public class DuckDuckGoHomePage extends PageObject {

    public static Target SEARCH_BOX = Target.the("DuckDuckGo main search field")
            .locatedBy("#search_form_input_homepage");

    public static Target searchBox() {
        return SEARCH_BOX;
    }
}
