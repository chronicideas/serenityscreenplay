package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import org.openqa.selenium.Keys;

import static net.serenitybdd.screenplay.Tasks.instrumented;
import static screenplay.pages.DuckDuckGoHomePage.SEARCH_BOX;

public class Search implements Task {

    private static String keywords;

    @Step("#actor searches for {0}")
    public static Search forTerm(String searchTerm) {
        keywords = searchTerm;
        return instrumented(Search.class);
    }

    @Step
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(keywords + Keys.RETURN)
        .into(SEARCH_BOX));
    }
}
