package screenplay.tasks;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.thucydides.core.annotations.Step;
import screenplay.actions.Select;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class ViewThe implements Task {

    @Step("#actors views the first search result")
    public static ViewThe firstSearchResult() {
        return instrumented(ViewThe.class);
    }


    @Step
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Select.theFirstResult());
    }
}
