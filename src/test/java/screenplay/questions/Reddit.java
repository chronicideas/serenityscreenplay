package screenplay.questions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.abilities.BrowseTheWeb;
import screenplay.model.RedditHome;

public class Reddit implements Question<RedditHome> {

    @Override
    public RedditHome answeredBy(Actor actor) {
        String title = BrowseTheWeb.as(actor).getTitle();
        return new RedditHome(title);
    }

    public static Reddit loaded() {
        return new Reddit();
    }
}
