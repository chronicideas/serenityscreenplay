package screenplay.model;

public class RedditHome {

    private final String title;

    public RedditHome(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return String.format("{title='%s'}", title);
    }
}
