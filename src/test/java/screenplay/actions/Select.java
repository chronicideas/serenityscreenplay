package screenplay.actions;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Interaction;
import screenplay.pages.DuckDuckGoSearchResultsPage;

public class Select implements Interaction {

    private DuckDuckGoSearchResultsPage searchResultsPage;

    public static Select theFirstResult() {
        return new Select();
    }

    @Override
    public <T extends Actor> void performAs(T actor) {
        searchResultsPage.selectFirstResult();
    }
}
