Feature: Searching for stuff on DuckDuckGo

  Scenario: 01. Searching and navigating to Reddit homepage
    Given Thomas is able to use DuckDuckGo for searching
    When he searches for "Reddit homepage"
    And he views the first result
    Then he should be on the Reddit homepage
